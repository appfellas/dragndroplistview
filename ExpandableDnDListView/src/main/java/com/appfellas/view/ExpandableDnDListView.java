package com.appfellas.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.appfellas.interfaces.ReplaceableItems;
import com.appfellas.listeners.DragListener;
import com.appfellas.listeners.DropListener;

/**
 * Created by Nikita Grishko on 24.08.13.
 * based on Eric Harlow's Drag and Drop ListView
 * include original Drag, Drop and Remove Listeners from DragNDrop ListView Project
 */
public class ExpandableDnDListView extends ExpandableListView {

    private static final String TAG = "ExpandableDnDListView";
    boolean mDragMode;
    int groupCount;
    int mStartPosition;
    int mEndPosition;
    int mDragPointOffset; // Used to adjust drag view location
    int backgroundColor;
    ImageView mDragView;
    DropListener tempListener;
    int grabberId;
    private Rect mTempRect = new Rect();


    public ExpandableDnDListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setAdapter(ExpandableListAdapter adapter) {
        super.setAdapter(adapter);

        this.setDropListener(new DropListener() {
            @Override
            public void onDrop(int from, int to) {
                if (getExpandableListAdapter() != null) {
                    ((ReplaceableItems) getExpandableListAdapter()).onReplace(from, to);
                } else {
                    Log.e(TAG, "ListAdapter is null");
                }
            }
        });
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(adapter);

        this.setDropListener(new DropListener() {
            @Override
            public void onDrop(int from, int to) {
                if (getExpandableListAdapter() != null) {
                    ((ReplaceableItems) getExpandableListAdapter()).onReplace(from, to);
                } else {
                    Log.e(TAG, "ListAdapter is null");
                }
            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        final int x = (int) ev.getX();
        final int y = (int) ev.getY();

        if (checkCollapseAll()&&action == MotionEvent.ACTION_DOWN &&isGrabberTouched(x,y)){

            mDragMode = true;
            Log.d(TAG, "DragMode = " + mDragMode);
        }

        if (!mDragMode)
            return super.onTouchEvent(ev);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                //collapseAll();
                mStartPosition = pointToPosition(x, y);
                if (mStartPosition != INVALID_POSITION) {
                    int mItemPosition = mStartPosition - getFirstVisiblePosition();
                    mDragPointOffset = y - getChildAt(mItemPosition).getTop();
                    mDragPointOffset -= ((int) ev.getRawY()) - y;
                    startDrag(mItemPosition, y);
                    drag(0, y);// replace 0 with x if desired
                }
                break;
            case MotionEvent.ACTION_MOVE:
                drag(0, y);// replace 0 with x if desired
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
            default:
                mDragMode = false;
                mEndPosition = pointToPosition(x, y);
                stopDrag(mStartPosition - getFirstVisiblePosition());
                if (mDropListener != null && mStartPosition != INVALID_POSITION
                        && mEndPosition != INVALID_POSITION)
                    mDropListener.onDrop(mStartPosition, mEndPosition);
                break;
        }
        return true;
    }

    // move the drag view
    private void drag(int x, int y) {
        if (mDragView != null) {
            WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) mDragView
                    .getLayoutParams();
            layoutParams.x = x;
            layoutParams.y = y - mDragPointOffset;
            WindowManager mWindowManager = (WindowManager) getContext()
                    .getSystemService(Context.WINDOW_SERVICE);
            mWindowManager.updateViewLayout(mDragView, layoutParams);

            if (mDragListener != null) {
                mDragListener.onDrag(x, y, this);// change null to "this" when
                // ready to use
            }
        }
    }

    // enable the drag view for dragging
    private void startDrag(int itemIndex, int y) {

        stopDrag(itemIndex);

        View item = getChildAt(itemIndex);
        if (item == null)
            return;
        item.setDrawingCacheEnabled(true);
        if (mDragListener != null)
            mDragListener.onStartDrag(item);

        // Create a copy of the drawing cache so that it does not get recycled
        // by the framework when the list tries to clean up memory
        Bitmap bitmap = Bitmap.createBitmap(item.getDrawingCache());

        WindowManager.LayoutParams mWindowParams = new WindowManager.LayoutParams();
        mWindowParams.gravity = Gravity.TOP;
        mWindowParams.x = 0;
        mWindowParams.y = y - mDragPointOffset;

        mWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mWindowParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mWindowParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        mWindowParams.format = PixelFormat.TRANSLUCENT;
        mWindowParams.windowAnimations = 0;

        Context context = getContext();
        ImageView v = new ImageView(context);
        v.setImageBitmap(bitmap);

        WindowManager mWindowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        mWindowManager.addView(v, mWindowParams);
        mDragView = v;
    }

    // destroy drag view
    private void stopDrag(int itemIndex) {

        if (mDragView != null) {
            if (mDragListener != null)
                mDragListener.onStopDrag(getChildAt(itemIndex));
            mDragView.setVisibility(GONE);
            WindowManager wm = (WindowManager) getContext().getSystemService(
                    Context.WINDOW_SERVICE);
            wm.removeView(mDragView);
            mDragView.setImageDrawable(null);
            mDragView = null;
        }
    }

    /*Use this method in your Activity to set GroupCount from your adapter*/
    public void setGroupCount(int count) {
        this.groupCount = count;
        if (this.getExpandableListAdapter() != null) {
            this.groupCount = this.getExpandableListAdapter().getGroupCount();
        }
    }
    /*this method get the id from users layout*/
    public void setGrabberId(int id){
        this.grabberId =id;
    }

    /*this method returns boolean variable as a result of checking if some of groups expand.
    *if it return false its mean that some of groups or all groups are expand, and we have to use
     * super in onTouchEvent in order to avoid problems while drag items */
    private boolean checkCollapseAll() {

        int check = 0;

        for (int j = 0; j < this.groupCount; j++) {
            if (!isGroupExpanded(j)) {
                check = check + 1;
            }
        }
        if (check != this.groupCount) {
            return false;
        } else {
            return true;
        }
    }
     /*This method take care about touching only grabber item in layout*/
    private boolean isGrabberTouched(int xPosition, int yPosition){
        int itemNum = pointToPosition(xPosition, yPosition);
        View item = (View) getChildAt(itemNum - getFirstVisiblePosition());
        View grabberView = item.findViewById(grabberId);
        Rect r = mTempRect;
        r.left=grabberView.getLeft();
        r.right=grabberView.getRight();
        r.top=grabberView.getTop();
        r.bottom=grabberView.getBottom();

        if ((r.left<xPosition) && (xPosition<r.right)){return true;}
        else {return false;}
    }




    /*This method collapse all expanded groups , but he gives very strange bug,
    some of items disappeared, and its happens very random*/
    /* private void collapseAll(){

        int ids =  ((ExpandableAdapter)getExpandableListAdapter()).getGroupCount();
        for (int j=0;j<ids;j++){
            if(isGroupExpanded(j)){
                collapseGroup(j);
            }
        }
    }*/


    /*This method set the background color for drag item, must be called from Activity */
    public void setBackgroundColor(int backColor) {
        this.backgroundColor = backColor;
    }

    public void setDropListener(DropListener dl) {
        this.tempListener = dl;
    }


    //------------------------------------------------------------------------
    /*Implementation of Drag and Drop Listeners inside the view */
    private DropListener mDropListener = new DropListener() {
        public void onDrop(int from, int to) {

            if (tempListener != null) {
                tempListener.onDrop(from, to);
            } else {
                Log.e(TAG, "DragListener is null");
            }

            invalidateViews();

        }
    };

    /*private RemoveListener mRemoveListener = new RemoveListener() {
        public void onRemove(int which) {

            invalidateViews();
        }
    };*/


    private DragListener mDragListener = new DragListener() {


        int defaultBackgroundColor;

        public void onDrag(int x, int y, ListView listView) {
            // TODO Auto-generated method stub
        }

        public void onStartDrag(View itemView) {
            itemView.setVisibility(View.INVISIBLE);
            defaultBackgroundColor = itemView.getDrawingCacheBackgroundColor();
            itemView.setBackgroundColor(backgroundColor);

        }

        public void onStopDrag(View itemView) {
            itemView.setVisibility(View.VISIBLE);
            itemView.setBackgroundColor(defaultBackgroundColor);


        }

    };


}


