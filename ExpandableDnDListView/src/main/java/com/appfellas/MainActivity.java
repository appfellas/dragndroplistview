package com.appfellas;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

import com.appfellas.adapter.ExpandableAdapter;
import com.appfellas.listeners.DropListener;
import com.appfellas.view.ExpandableDnDListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {
    ExpandableAdapter listAdapter;
    ExpandableDnDListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        expListView = (ExpandableDnDListView) findViewById(R.id.exLv);
        /*set Background Color */
        expListView.setBackgroundColor(getResources().getColor(R.color.blue));
        // preparing list data
        prepareListData();
        /*Don't forget to implement ReplaceableItems inside of your Expandable adapter!!!*/
        listAdapter = new ExpandableAdapter(this, listDataHeader, listDataChild);
        /*setGroupCount from your adapter, its real important don't forget about this!*/
        expListView.setGroupCount(listAdapter.getGroupCount());
        /*Set id of your grabber*/
        expListView.setGrabberId(R.id.ivGrabber);
        /*synchronizes your view and adapter when item is drop by set DropListener */
        expListView.setDropListener(new DropListener() {
            @Override
            public void onDrop(int from, int to) {
                listAdapter.onReplace(from, to);
            }
        });


        /*And the last step, set the adapter!*/
        expListView.setAdapter(listAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Top 250");
        listDataHeader.add("Now Showing");
        listDataHeader.add("Coming Soon..");
        listDataHeader.add("Comics");


        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");
        top250.add("The Godfather: Part II");
        top250.add("Pulp Fiction");
        top250.add("The Good, the Bad and the Ugly");
        top250.add("The Dark Knight");
        top250.add("12 Angry Men");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("The Conjuring");
        nowShowing.add("Despicable Me 2");
        nowShowing.add("Turbo");
        nowShowing.add("Grown Ups 2");
        nowShowing.add("Red 2");
        nowShowing.add("The Wolverine");

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");

        List<String> comics = new ArrayList<String>();
        comics.add("Thor 2");
        comics.add("Superman and Batman");
        comics.add("X-Man");
        comics.add("Avengers 2 ");
        comics.add("Justice League");

        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nowShowing);
        listDataChild.put(listDataHeader.get(2), comingSoon);
        listDataChild.put(listDataHeader.get(3), comics);
    }


}

