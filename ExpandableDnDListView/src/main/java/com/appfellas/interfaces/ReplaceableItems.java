package com.appfellas.interfaces;

/**
 * Created by 2faces on 26.08.13.
 * Must be implement inside ExpandableAdapter
 */
public interface ReplaceableItems {

    /**
     * Called when an item is to be dropped.
     * @param from - index item replace from.
     * @param to - index replace to.
     */
    void onReplace(int from, int to);
}

